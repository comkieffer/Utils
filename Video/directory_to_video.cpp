
#include <iostream>

// Waiting for C++17 support on Ubuntu 16.04 :snif:
#include <boost/optional.hpp>
#include <boost/filesystem.hpp>

#include <clara/clara.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace clara;
namespace fs = boost::filesystem;

void handle_input(const fs::path& input, std::vector<fs::path>& all_files) {
    if (fs::is_regular( input )) {
        all_files.emplace_back(input);
    } else if (fs::is_directory( input )) {
        // The directory_iterator returns the files in an unsorted order. This is the opposite of
        // what we want since the order of the frames is important.
        // The workaround is to store the filenames in a new vector that we will sort before
        // appending onto the old one.
        std::vector<fs::path> files_in_this_directory;

        for (const auto& file: fs::directory_iterator{ input }) {
            if (fs::is_regular(file))
                files_in_this_directory.emplace_back(file);
        }

        std::sort(files_in_this_directory.begin(), files_in_this_directory.end());
        std::copy(files_in_this_directory.begin(), files_in_this_directory.end(), std::back_inserter(all_files));
    }
}

cv::Mat read_image(const fs::path& file) {
    const cv::Mat image = cv::imread(file.string());
    if (image.empty()) {
        std::cerr << "Read error whilst reading '" << file << "'. Unable to continue.\n";
        exit(5);
    }

    return image;
}

int main(int argc, char** argv) {

    // I/O Options
    bool showhelp = false;
    std::string output_file{ "video.avi" };
    std::vector<std::string> inputs;

    // Video Options
    double videoFPS = 24.0;
    std::string codec{ "FFV1" };

    auto cli
        = Opt(output_file, "output-file") ["-o"] ["--output-file"]
            ("The filename of the generated video")
        | Opt(videoFPS, "fps") ["-f"] ["--fps"]
            ("The framerate of the final video")
        | Opt(codec, "Codec") ["-c"] ["--codec"]
            ("The codec to use for video compression. See www.fourcc.org for a full list. ")
        | Arg(inputs, "input-files")
            ("The input files or folders")
        | Help(showhelp);

    auto result = cli.parse( Args{argc, argv} );
    if (!result) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
        cli.writeToStream(std::cout);
        exit(1);
    }

    if (showhelp) {
        cli.writeToStream(std::cout);
        return 0;
    }

    if (inputs.empty()) {
        std::cout << "No input files. Nothing to do\n";
        exit(0);
    }

    if (codec.size() != 4) {
        std::cerr << "Error in command line: the codec should be a four letter string corresponding to one on the "
                  << "FourCC codecs. See www.fourcc.org for a complete list of codecs. Common codecs that are likely "
                     "to work on your system are 'MJPG' and 'H264'\n";
        exit(3);
    }

    std::cout << "- Building file list ...\n";
    std::vector<fs::path> all_input_files;
    for (const auto& input: inputs) {
        handle_input({ input }, all_input_files);
    }

    if (all_input_files.empty()) {
        std::cerr << "Unable to locate any regular files in the list of input files. \n"
                     "Nothing to do.\n";
        exit(4);
    }

    std::cout << "- Initialising video writer ...\n";
    const cv::Size image_size = read_image(all_input_files.front()).size();
    const auto fourcc = cv::VideoWriter::fourcc(codec[0], codec[1], codec[2], codec[3]);

    cv::VideoWriter video(output_file, fourcc, videoFPS, image_size);
    if (!video.isOpened()) {
        std::cerr << "Unable to open VideoWrite object. Cannot proceed.\n";
        exit(7);
    }

    std::cout << "- Processing image files ...\n";
    for (const fs::path& file: all_input_files) {
        video << read_image(file);
    }
    std::cout << "All Done. \n";

    return 0;
}
